// AH 26set19 

const date = new Date()
const fullYear = date.getFullYear()

document.getElementById("selectYear").value = fullYear

function leapYear(year) {
  if (year % 400 === 0 || (year % 4 === 0 && year % 100 !== 0))
   document.getElementsByTagName("span")[0].style.display = "none"
  else
   document.getElementsByTagName("span")[0].style.display = "inline"
}

leapYear(fullYear)